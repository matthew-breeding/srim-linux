# SRIM for Linux

This is a containerized version of SRIM that runs on local/personal 
Linux machines as well as HPC clusters (e.g. ACCRE @ Vanderbilt)


Install 
=====================================================================
Clone the git repository (`git clone git@gitlab.com:matthew-breeding/srim-linux.git`)
or download the zipped repository and extract. Then to install using 
default settings:

`chmod +x configure`

`./configure`

This will install the srim executable at `$HOME/.local/bin`, the srim 
singularity image at `$HOME/.local/include`. If `$HOME/.local/bin` is 
not included in your `$PATH` variable, the configure script will prompt
you to add it to your `~/.bashrc` or `~/.bash_profile`. 

The SRIM directory is located by default at `$HOME/.srim`. This is where
the singularity container is executed, all of the SRIM files unzipped, 
and where the output files are located. 

`./configure --help` for detailed options on changing these paths

*** Requires X11 and singularity to be installed. Visit https://sylabs.io 
for info/ download ***

=====================================================================

Visit http://srim.org for more info on SRIM
